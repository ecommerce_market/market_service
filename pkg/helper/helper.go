package helper

import (
	"crypto/rand"
	"database/sql"
	"fmt"
	"strconv"
	"strings"
)

func ReplaceQueryParams(namedQuery string, params map[string]interface{}) (string, []interface{}) {
	var (
		i    int = 1
		args []interface{}
	)

	for k, v := range params {
		if k != "" {
			namedQuery = strings.ReplaceAll(namedQuery, ":"+k, "$"+strconv.Itoa(i))

			args = append(args, v)
			i++
		}
	}

	return namedQuery, args
}

func ReplaceSQL(old, searchPattern string) string {
	tmpCount := strings.Count(old, searchPattern)
	for m := 1; m <= tmpCount; m++ {
		old = strings.Replace(old, searchPattern, "$"+strconv.Itoa(m), 1)
	}
	return old
}

const otpChars = "1234567890"

func GenerateOTP(length int) (string, error) {
	buffer := make([]byte, length)
	_, err := rand.Read(buffer)
	if err != nil {
		return "", err
	}

	otpCharsLength := len(otpChars)
	for i := 0; i < length; i++ {
		buffer[i] = otpChars[int(buffer[i])%otpCharsLength]
	}

	return string(buffer), nil
}

func Difference(a, b []int32) []int32 {
	mb := make(map[int32]struct{}, len(b))
	for _, x := range b {
		mb[x] = struct{}{}
	}
	var diff []int32
	for _, x := range a {
		if _, found := mb[x]; !found {
			diff = append(diff, x)
		}
	}
	return diff
}

func ValMultipleQuery(query string, vals []int32) (string, []interface{}) {
	params := []interface{}{}

	for i, id := range vals {
		query += fmt.Sprintf("$%d,", i+1)
		params = append(params, id)
	}

	query = query[:len(query)-1] // remove trailing ","
	query += ")"

	return query, params
}

func InsertMultiple(queryInsert string, id int32, vals []int32) (string, []interface{}) {
	insertparams := []interface{}{}

	for i, d := range vals {
		p1 := i * 2 // starting position for insert params
		queryInsert += fmt.Sprintf("($%d, $%d),", p1+1, p1+2)

		insertparams = append(insertparams, d, id)
	}

	queryInsert = queryInsert[:len(queryInsert)-1] // remove trailing ","

	return queryInsert, insertparams
}

func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

func NewNullBool(s bool) sql.NullBool {
	if !s {
		return sql.NullBool{}
	}
	return sql.NullBool{
		Bool:  s,
		Valid: true,
	}
}

// func GenerateExcelFile(role string) (*excelize.File, error) {
// 	file := excelize.NewFile()

// 	// Add a sheet
// 	index := file.NewSheet("Sheet1")

// 	// Add headers

// 	file.SetCellValue("Sheet1", "A1", "first_name")
// 	file.SetCellValue("Sheet1", "B1", "last_name")
// 	file.SetCellValue("Sheet1", "C1", "phone")
// 	file.SetCellValue("Sheet1", "D1", "email")
// 	file.SetCellValue("Sheet1", "E1", "role")
// 	file.SetCellValue("Sheet1", "F1", "create_at")
// 	file.SetCellValue("Sheet1", "G1", "upadated_at")
// 	var cfg = config.Load()
// 	strg, err := postgres.NewConnectionPostgres(&cfg)
// 	if err != nil {
// 		panic(err)
// 	}

// 	user, err := strg.User().GetList(context.Background(), &models.GetListUserRequest{
// 		Offset: 0,
// 		Limit:  10000,
// 		Search: role,
// 	})
// 	if err != nil {
// 		return nil, err
// 	}
// 	for i, v := range user.Users {
// 		file.SetCellValue("Sheet1", fmt.Sprintf("A%d", i+2), v.FirstName)
// 		file.SetCellValue("Sheet1", fmt.Sprintf("B%d", i+2), v.LastName)
// 		file.SetCellValue("Sheet1", fmt.Sprintf("C%d", i+2), v.Phone)
// 		file.SetCellValue("Sheet1", fmt.Sprintf("D%d", i+2), v.Email)
// 		file.SetCellValue("Sheet1", fmt.Sprintf("E%d", i+2), v.Role)
// 		file.SetCellValue("Sheet1", fmt.Sprintf("F%d", i+2), v.CreatedAt)
// 		file.SetCellValue("Sheet1", fmt.Sprintf("G%d", i+2), v.UpdatedAt)

// 	}
// 	// Set active sheet
// 	file.SetActiveSheet(index)

// 	return file, nil
// }
