package handler

import (
	"context"
	"fmt"
	"market_service/config"
	"market_service/models"
	"market_service/pkg/helper"
	"math/rand"
	"net/http"
	"net/smtp"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func (h *Handler) SendOTP(c *gin.Context) {
	var email = c.Query("email")
	var createforget models.ForgetPasswords
	from := "abduqodirmusayeff@gmail.com"
	password := "mlqm occc qztv mtko"
	to := []string{
		email,
	}
	smtpHost := "smtp.gmail.com"
	smtpPort := "587"
	msg := rand.Intn(90000) + rand.Intn(10000)
	code := strconv.Itoa(msg)
	message := []byte(code)

	auth := smtp.PlainAuth("", from, password, smtpHost)
	stringmsg := fmt.Sprintf("%v", msg)
	createforget.Code = stringmsg
	createforget.Email = email

	existingUser, err := h.strg.OTP().GetByEmail(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	if err == nil && existingUser != nil {
		c.JSON(http.StatusBadRequest, "user already exists please login")
		return
	}

	resp, err := h.strg.OTP().GetByEmail(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	fmt.Println("?????", resp)
	if err != nil && err.Error() != "no rows in result set" {
		c.JSON(500, err.Error())
		return
	}

	if resp == nil {
		err = h.strg.OTP().Create(context.Background(), &createforget)
		if err != nil {
			fmt.Println("info>>>>>>>>>", resp)
			c.JSON(500, resp)
			return
		}
		err = smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
		if err != nil {
			c.JSON(500, err.Error())
			return
		}
		c.JSON(200, email)
		return
	} else {
		err = h.strg.OTP().Delete(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})

		if err != nil {
			c.JSON(500, err.Error())
			return
		}
		err = smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)

		if err != nil {
			c.JSON(404, err.Error())
			return
		}
		c.JSON(200, email)
		return
	}
}
func (h *Handler) Verify(c *gin.Context) {
	email := c.Query("email")
	code := c.Query("code")

	resp, err := h.strg.OTP().GetByEmail(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	if err != nil {
		fmt.Println("Error getting OTP by email:", err)
		c.JSON(http.StatusBadRequest, err)
		return
	}
	fmt.Println("OTP response:", resp)

	if resp.Code != code {
		c.JSON(http.StatusBadRequest, "wrong code")
		return
	}

	err = h.strg.OTP().Delete(context.Background(), &models.ForgetPasswordPrimaryKey{Email: email})
	if err != nil {
		fmt.Println("Error deleting OTP:", err)
		c.JSON(500, err.Error())
		return
	}

	var req models.VerifyOTPRequest

	user, err := h.strg.User().GetByEmail(context.Background(), &models.UserPrimaryKey{
		Email: email,
	})
	if err != nil {
		if err.Error() == "no rows in result set" {
			resp := models.AccessToken{
				Token:     "",
				UserFound: false,
				Name:      "",
				Email:     req.Email,
				UserId:    "",
			}
			c.JSON(http.StatusOK, resp)
			return
		} else {
			c.JSON(404, err.Error())
			return
		}
	}

	data := map[string]interface{}{
		"id":   user.Id,
		"role": user.Role,
	}

	token, err := helper.GenerateJWT(data, config.TimeExpiredAt, h.cfg.SecretKey)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	respp := models.AccessToken{
		Token:     token,
		UserFound: true,
		Name:      user.Name,
		Email:     req.Email,
		UserId:    user.Id,
	}

	c.JSON(200, respp)
}

func (h *Handler) Login(c *gin.Context) {

	email := c.Query("email")
	password := c.Query("password")

	user, err := h.strg.User().GetByEmail(context.Background(), &models.UserPrimaryKey{Email: email})
	if err != nil {
		fmt.Println("11111111111111111111", user)
		c.JSON(500, err.Error())
		return
	}

	hashedPassword := user.Password // Store the original hashed password

	user.Password = "" // Clear the password in the user struct

	err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		c.JSON(500, "login or password wrong")
		return
	}

	data := map[string]interface{}{
		"id":   user.Id,
		"role": user.Role,
	}

	token, err := helper.GenerateJWT(data, config.TimeExpiredAt, h.cfg.SecretKey)
	if err != nil {
		c.JSON(404, "token error")
		return
	}

	c.JSON(200, models.LoginResponse{UserData: user, Token: token})
}
