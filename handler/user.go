package handler

import (
	"context"
	"fmt"
	"market_service/config"
	"market_service/models"
	"net/http"

	// "market_service/pkg/helper"
	"market_service/storage/postgres"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func (h *Handler) CreateUser(c *gin.Context) {
	var user models.CreateUser
	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, "failed to marshall json file")
		return
	}
	var cfg = config.Load()
	strg, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}
	existingUser, err := h.strg.User().GetByEmail(context.Background(), &models.UserPrimaryKey{Email: user.Email})
	if err == nil && existingUser != nil {
		c.JSON(http.StatusBadRequest, "user already exists please login")
		return
	}

	if len(user.Password) < 8 {
		c.JSON(http.StatusBadRequest, "password must be greater than 8 characters")
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 9)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	user.Password = string(hashedPassword)
	resp, err := strg.User().Create(context.Background(), &user)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	resp.Password = ""
	c.JSON(200, resp)
}



func (h *Handler) GetUserList(c *gin.Context) {
	fmt.Println("logggggggggggggg")
	offset := c.Query("offset")
	limit := c.Query("limit")
	// search := "student"
	offsetInt, err := strconv.Atoi(offset)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	limitInt, err := strconv.Atoi(limit)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	resp, err := h.strg.User().GetList(context.Background(), &models.GetListUserRequest{Offset: int64(offsetInt), Limit: int64(limitInt)})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	for _, v := range resp.Users {
		v.Password = ""
	}
	c.JSON(200, resp)
}

func (h *Handler) GetUserByEmail(c *gin.Context) {
	email := c.Param("email")
	resp, err := h.strg.User().GetByEmail(context.Background(), &models.UserPrimaryKey{Email: email})
	if resp == nil && err.Error() != "no rows in result set" {
		c.JSON(500, err)
		return
	}
	if resp == nil {
		c.JSON(404, "user not found")
		return
	}

	resp.Password = ""
	c.JSON(200, resp)
}

func (h *Handler) GetUserByID(c *gin.Context) {
	fmt.Println(c.Params)
	id := c.Param("id")
	fmt.Println(id)
	resp, err := h.strg.User().GetByID(context.Background(), &models.UserPrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	resp.Password = ""
	c.JSON(200, resp)
}

func (h *Handler) DeleteUser(c *gin.Context) {
	id := c.Param("id")

	err := h.strg.User().Delete(context.Background(), &models.UserPrimaryKey{Id: id})
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "success")
}

// func (h *Handler) GenerateExcelStudent(c *gin.Context) {
// 	// Generate the Excel file
// 	file, err := helper.GenerateExcelFile("student")
// 	if err != nil {
// 		c.JSON(500, gin.H{"error": err.Error()})
// 		return
// 	}

// 	// Save the file to a specific location on the server
// 	filePath := "/abduqodir/code/ulab/lms_service/" // Replace with your desired file path
// 	err = file.SaveAs(filePath)
// 	if err != nil {
// 		c.JSON(500, gin.H{"error": err.Error()})
// 		return
// 	}

// 	// Respond with the file path or any other necessary information
// 	c.JSON(200, gin.H{"file_path": filePath})
// }

// func (h *Handler) GenerateExcelStudent(c *gin.Context) {
// 	// Generate the Excel file
// 	file, err := helper.GenerateExcelFile("student")
// 	if err != nil {
// 		c.JSON(500, gin.H{"error": "Failed to generate Excel file"})
// 		return
// 	}

// 	// Specify the directory to save the file (adjust this according to your needs)
// 	saveDirectory := "/abduqodir/code/ulab/lms_service/"
// 	fmt.Println("Save Directory3:", saveDirectory)

// 	// Create the directory if it doesn't exist
// 	if err := os.MkdirAll(saveDirectory, 0755); err != nil {
// 		c.JSON(500, gin.H{"error": "Failed to create directory"})
// 		return
// 	}
// 	fmt.Println("Save Directory1:", saveDirectory)

// 	// Generate a unique filename
// 	fileName := fmt.Sprintf("file_%s.xlsx", time.Now().Format("20060102150405"))

// 	// Construct the full file path
// 	filePath := filepath.Join(saveDirectory, fileName)
// 	fmt.Println("Save Directory2:", saveDirectory)

// 	// Save the file to the specified location
// 	err = file.SaveAs(filePath)
// 	if err != nil {
// 		c.JSON(500, gin.H{"error": "Failed to save Excel file"})
// 		return
// 	}

// 	// Respond with the file path or any other necessary information
// 	c.JSON(200, gin.H{"file_path": filePath})
// }

// func (*h Handler) GenerateExcelTeacher(){

// }
