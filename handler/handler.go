package handler

import (
	"market_service/config"
	"market_service/storage"
)

type Handler struct {
	cfg  *config.Config
	strg storage.StorageI
}

func NewHandler(cfg *config.Config, strg storage.StorageI) *Handler {
	return &Handler{
		cfg:  cfg,
		strg: strg,
	}
}
