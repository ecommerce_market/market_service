package models

type VerifyOTPRequest struct {
	Code  string `json:"code"`
	Email string `json:"email"`
}

type AccessToken struct {
	Token     string `json:"token"`
	Email     string `json:"email"`
	UserFound bool   `json:"user_found"`
	Name      string `json:"name"`
	UserId    string `json:"user_id"`
}

type LoginResponse struct {
	UserData *User  `json:"user_data"`
	Token    string `json:"token"`
}
