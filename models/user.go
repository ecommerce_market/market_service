package models

type User struct {
	Id            string `json:"id"`
	Name          string `json:"name"`
	Email         string `json:"email"`
	PersonalImage string `json:"personal_image"`
	Password      string `json:"password"`
	Role          string `json:"role"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
}

type CreateUser struct {
	Name          string `json:"name"`
	Email         string `json:"email"`
	PersonalImage string `json:"personal_image"`
	Password      string `json:"password"`
	Role          string `json:"role"`
}

type UserPrimaryKey struct {
	Id    string `json:"id"`
	Email string `json:"email"`
}

type UpdateUser struct {
	Id            string `json:"id"`
	Name          string `json:"name"`
	Email         string `json:"email"`
	PersonalImage string `json:"personal_image"`
	Password      string `json:"password"`
	Phone         string `json:"phone"`
}

type GetListUserRequest struct {
	Offset int64  `json:"offset"`
	Limit  int64  `json:"limit"`
	Search string `json:"search"`
	Query  string `json:"query"`
}

type GetListUserResponse struct {
	Count int64   `json:"count"`
	Users []*User `json:"data"`
}
