package postgres

import (
	"context"
	"database/sql"
	"market_service/models"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

type OTPRepo struct {
	db *pgxpool.Pool
}

func NewOTPRepo(db *pgxpool.Pool) *OTPRepo {
	return &OTPRepo{
		db: db,
	}
}

func (r *OTPRepo) Create(ctx context.Context, req *models.ForgetPasswords) error {
	var (
		query = `
		INSERT INTO verification_code(
			email,
			code,
			expired_at
			)VALUES($1,$2,$3)
			`
	)
	_, err := r.db.Exec(
		ctx,
		query,
		req.Email,
		req.Code,
		time.Now().Add(time.Minute),
	)
	if err != nil {
		return nil
	}
	return nil
}

func (r *OTPRepo) GetByEmail(ctx context.Context, req *models.ForgetPasswordPrimaryKey) (*models.Forget, error) {
	var (
		query = `
		SELECT 
		email,
		code,
		created_at,
		expired_at
		FROM verification_code
		WHERE email=$1
		
		`
	)
	var (
		email      sql.NullString
		code       sql.NullString
		created_at sql.NullString
		expired_at sql.NullString
	)

	err := r.db.QueryRow(ctx, query, req.Email).Scan(
		&email,
		&code,
		&created_at,
		&expired_at,
	)

	if err != nil {
		return nil, err
	}
	return &models.Forget{
		Email:     email.String,
		Code:      code.String,
		CreatedAt: created_at.String,
		ExpiredAt: expired_at.String,
	}, nil

}

func (r *OTPRepo) Delete(ctx context.Context, req *models.ForgetPasswordPrimaryKey) error {
	_, err := r.db.Exec(ctx, `DELETE FROM verification_code WHERE email = $1`, req.Email)
	return err
}
