package postgres

import (
	"context"
	"fmt"
	"market_service/config"
	"market_service/storage"

	"github.com/jackc/pgx/v4/pgxpool"
	
)

type Store struct {
	db     *pgxpool.Pool
	user   *UserRepo
	otp    *OTPRepo
}

func NewConnectionPostgres(cfg *config.Config) (storage.StorageI, error) {

	config, err := pgxpool.ParseConfig(

		fmt.Sprintf(
			"host=%s user=%s dbname=%s password=%s port=%s ",
			cfg.PostgresHost,
			cfg.PostgresUser,
			cfg.PostgresDatabase,
			cfg.PostgresPassword,
			cfg.PostgresPort,
		),
	)
	if err != nil {
		return nil, err
	}
	config.MaxConns = cfg.PostgresMaxConnection
	pgxpool, err := pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		panic(err)
	}
	return &Store{
		db: pgxpool,
	}, nil
}



func (s *Store) User() storage.UserRepoI {

	if s.user == nil {
		s.user = NewUserRepo(s.db)
	}

	return s.user
}

func (s *Store) OTP() storage.OTPRepoI {

	if s.otp == nil {
		s.otp = NewOTPRepo(s.db)
	}

	return s.otp
}