package storage

import (
	"context"
	"market_service/models"
)

type StorageI interface {
	User() UserRepoI
	OTP() OTPRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *models.CreateUser) (*models.User, error)
	GetByID(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error)
	Update(ctx context.Context, req *models.UpdateUser) (*models.User, error)
	GetByEmail(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error)
	GetList(ctx context.Context, req *models.GetListUserRequest) (*models.GetListUserResponse, error)
	Delete(ctx context.Context, req *models.UserPrimaryKey) error
}

type OTPRepoI interface {
	Create(ctx context.Context, req *models.ForgetPasswords) error
	GetByEmail(ctx context.Context, req *models.ForgetPasswordPrimaryKey) (*models.Forget, error)
	Delete(ctx context.Context, req *models.ForgetPasswordPrimaryKey) error
}