create table if not exists "roles_and_permissions" (
                                                     id serial primary key,
                                                     name varchar(40) not null unique,
                                                     description varchar(255),
                                                     parent int,
                                                     is_role bool not null default false
);

create table if not exists "user" (
                                     id serial primary key,
                                     name varchar(30) not null,
                                     phone_number varchar(12) unique,
                                     email varchar(255) unique,
                                     role_id int not null default 2,
                                     personal_image varchar(255),

                                     created_at timestamp,
                                     updated_at timestamp,
                                 foreign key(role_id) references roles_and_permissions (id) on delete set default
);

alter table "user"
add column verified bool default false;

create table if not exists "user_address" (
    id serial primary key,
    user_id int not null,
    long float not null,
    lat float not null,
    name varchar(30),
    foreign key (user_id) references "user"(id)
);