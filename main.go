package main

import (
	"market_service/config"
	"market_service/handler"
	"market_service/storage/postgres"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	var cfg = config.Load()
	strg, err := postgres.NewConnectionPostgres(&cfg)
	if err != nil {
		panic(err)
	}
	handler := handler.NewHandler(&cfg, strg)
	r := gin.Default()
	r.Use(CORSMiddleware())

	//routes
	r.POST("/api/v1/register", handler.CreateUser)
	r.POST("/api/v1/sendcode", handler.SendOTP)
	r.POST("/api/v1/login", handler.Login)
	r.POST("/api/v1/verify", handler.Verify)
	r.GET("/api/v1/userbyemail/:email", handler.GetUserByEmail)

	// student routes
	r.POST("/api/v1/users", handler.CreateUser)
	r.GET("/api/v1/users/:id", handler.GetUserByID)
	r.GET("/api/v1/users", handler.GetUserList)
	r.DELETE("/api/v1/users", handler.DeleteUser)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if err := r.Run(":" + port); err != nil {
		log.Panicf("error: %s", "panic")
	}
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE, PATCH")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}